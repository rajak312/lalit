import { HomeScreen } from 'app/screens/home'
import Head from 'next/head'

export default function Page() {
  return (
    <>
      <Head>
        <title>Lalit Rajak</title>
      </Head>
      <HomeScreen />
    </>
  )
}
