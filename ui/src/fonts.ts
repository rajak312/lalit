import { createInterFont } from '@tamagui/font-inter'
import type { GenericFont } from '@tamagui/web'
import { createFont } from '@tamagui/web'
import 'typeface-lobster'
import 'typeface-playfair-display'

const defaults = {
  size: {
    1: 11,
    2: 12,
    3: 13,
    4: 14,
    5: 15,
    6: 16,
    7: 18,
    8: 21,
    9: 28,
    10: 42,
    11: 52,
    12: 62,
    13: 72,
    14: 92,
    15: 114,
    16: 124,
  } as const,
  letterSpacing: {
    4: 1,
    5: 3,
    6: 3,
    9: -2,
    10: -3,
    12: -4,
  } as const,
  weight: {
    4: '300',
  } as const,
}

export function createDefaultFont<A extends GenericFont>(
  font: Partial<A> & { family: A['family'] }
): A {
  const size = font.size || defaults.size
  return createFont({
    lineHeight: Object.fromEntries(
      Object.entries(size).map(([k, v]) => [k, typeof v === 'number' ? v * 1.2 + 6 : v])
    ) as typeof size,
    letterSpacing: defaults.letterSpacing,
    weight: defaults.weight,
    ...font,
    size,
  } as unknown as A)
}

export const fonts = {
  playFlare: createDefaultFont({
    family: 'Playfair Display',
    weight: {
      400: '400',
      700: '700',
    },
    letterSpacing: {
      400: 1,
      700: 1.5,
    },
    style: {
      normal: 'normal',
      italic: 'italic',
    },
  }),
  lobster: createDefaultFont({
    family: 'Lobster',
  }),
  rockSalt: createDefaultFont({
    family: 'Rock Salt',
  }),
  silkscreen: createDefaultFont({
    family: 'Silkscreen',
  }),
  heading: createInterFont({
    size: {
      6: 15,
    },
    transform: {
      6: 'uppercase',
      7: 'none',
    },
    weight: {
      6: '400',
      7: '700',
    },
    color: {
      6: '$colorFocus',
      7: '$color',
    },
    letterSpacing: {
      5: 2,
      6: 1,
      7: 0,
      8: -1,
      9: -2,
      10: -3,
      12: -4,
      14: -5,
      15: -6,
    },
    face: {
      700: { normal: 'InterBold' },
    },
  }),
  body: createInterFont(
    {
      face: {
        700: { normal: 'InterBold' },
      },
    },
    {
      sizeSize: (size) => Math.round(size * 1.1),
      sizeLineHeight: (size) => Math.round(size * 1.1 + (size > 20 ? 10 : 10)),
    }
  ),
} as const
