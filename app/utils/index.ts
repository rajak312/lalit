export interface Profile {
  name: string
  yearOfExperience: number
  role?: string
  proficiency?: string
  // about: string[]
  socials?: {
    name: Social
    link: string
    displayName?: string
  }[]
  projects: {
    image: string
    name: string
    title: string
    description: string
    achievement: string
    technologyUsed: string[]
    category: string[]
    goal: string
    link?: string
    year?: string
    live?: boolean
    responsibilities: string[]
  }[]
  services: {
    name: string
    description: string
    logo: string
  }[]
  experience: {
    company: string
    logo: string
    url: string
    roles: {
      role: string
      from: string
      to?: string
      description: string
      skills?: string[]
    }[]
    from: string
    to?: string
    location: string
  }[]
}

export enum Social {
  LinkedIn = 'linkedin',
  Github = 'github',
  Gitlab = 'gitlab',
  Skype = 'skype',
  Discord = 'discord',
  Mail = 'mail',
}

export const data: Profile = {
  name: 'Lalit Kumar Rajak',
  yearOfExperience: 3,
  role: 'Full Stack Developer',
  proficiency: 'Proficient with Nest.js, Next.js, React, Java, Docker, Databases',
  socials: [
    {
      name: Social.Mail,
      link: 'mailto:lalitrajak312@gmail.com',
      displayName: 'lalitrajak312@gmail.com',
    },
    {
      name: Social.Github,
      link: 'https://github.com/rajak312',
    },
    {
      name: Social.LinkedIn,
      link: 'https://www.linkedin.com/in/rajak312/',
    },
    {
      name: Social.Gitlab,
      link: 'https://gitlab.com/rajak312',
    },
    {
      name: Social.Skype,
      link: 'skype:live:.cid.1ee0ce4d6626f098',
      displayName: 'live:.cid.1ee0ce4d6626f098',
    },
    {
      name: Social.Discord,
      link: 'https://discord.com/users/1177459370217439289',
      displayName: '1177459370217439289',
    },
  ],
  experience: [
    {
      company: 'FlyingFox Labs PVT LTD',
      logo: require('app/assets/flyingfox.png'),
      url: 'https://hq.flyingfoxlabs.in/',
      roles: [
        {
          role: 'Full Stack Developer',
          from: 'July 2024',
          // to: 'Present',
          description:
            'Worked on full stack projects combining frontend and backend technologies including TypeScript, React, Next.js, React Native, Nest.js, GraphQL, and Docker. Developed end-to-end solutions ensuring seamless integration between frontend and backend.',
        },
        {
          role: 'Backend Developer',
          from: 'August 2023',
          to: 'July 2024',
          description:
            'Built robust backends using Nest.js and GraphQL. Integrated REST and GraphQL APIs for seamless communication between frontend and backend systems. Utilized Docker for containerization.',
        },
        {
          role: 'Frontend Developer',
          from: 'December 2022',
          to: 'July 2023',
          description:
            'Developed dynamic frontends using TypeScript, Tamagui, React, and Next.js. Created custom components to enhance user interface and experience.',
          skills: ['TypeScript', 'React', 'Next.js', 'Tamagui'],
        },
      ],
      from: 'December 2022',
      // to: 'July 2023',
      location: 'Hyderabad, India',
    },
    {
      company: 'Naresh Institute of Technology',
      url: 'https://nareshit.com/',
      logo: require('app/assets/nareshit.png'),
      roles: [
        {
          role: 'Trainee',
          from: 'March 2022',
          to: 'November 2022',
          description:
            'Completed a comprehensive training program covering HTML, CSS, JavaScript, Bootstrap, TypeScript, Angular, Java, SQL, and Spring Boot. Gained hands-on experience in full stack development and best practices in modern web technologies.',
        },
      ],
      from: 'March 2022',
      to: 'November 2022',
      location: 'Hyderabad, India',
    },
  ],
  projects: [
    {
      name: 'zk-poker',
      image: require('app/assets/zkpoker.jpg'),
      title: 'zkPoker Online Gaming Platform',
      description: 'Online poker platform with a seamless gaming experience.',
      goal: 'Develop a seamless online poker platform that provides a real-time, interactive gaming experience for users.',
      achievement:
        'Created a real-time, interactive gaming solution that offers a smooth and engaging poker experience.',
      technologyUsed: ['React', 'Next.js', 'TypeScript', 'Zustand', 'Axios', 'NestJS'],
      category: ['Frontend', 'Backend'],
      link: 'https://outlawhideout.io/',
      year: '2022-Present',
      live: true,
      responsibilities: [
        'Developed API using NestJS for game mechanics',
        'Integrated API with frontend using Next.js and React',
        'Ensured seamless real-time data flow using Zustand for state management',
        'Handled frontend state management and user interactions with React',
        'Tested and debugged API endpoints to ensure smooth communication',
      ],
    },
    {
      name: 'quickcam',
      image: require('app/assets/quickcam.png'),
      title: 'QuickCam Camera',
      description: 'A comprehensive application for managing and accessing camera feeds.',
      goal: 'Develop a full-stack application with a robust backend and seamless frontend integration for camera feed management.',
      achievement:
        'Maintained the backend, created an API, managed MongoDB connections, maintained the frontend, and implemented Keycloak authentication.',
      technologyUsed: ['React', 'Redux', 'Node.js', 'NestJS', 'MongoDB', 'Keycloak'],
      category: ['Frontend', 'Backend'],
      link: 'https://promanager-quickcam-react-staging.promanager.online/',
      year: 'April 2024-present',
      live: true,
      responsibilities: [
        'Deployed frontend, backend, and MongoDB database',
        'Integrated Keycloak for authentication',
        'Handled DevOps tasks for deployment and integration',
        'Maintained MongoDB connections and queries',
        'Ensured frontend and backend integration for real-time camera feeds',
        'Managed infrastructure and ensured uptime during deployment',
      ],
    },
    {
      name: 'office-hours',
      image: require('app/assets/office-hours.webp'),
      title: 'Office-Hours Scheduling Platform',
      description: 'Comprehensive scheduling system for booking and managing time slots.',
      goal: 'Facilitate seamless booking and time management through a comprehensive scheduling system.',
      achievement:
        'Built a secure, decentralized scheduling system that allows efficient booking and time management.',
      technologyUsed: ['React', 'Tamagui', 'Next.js', 'Solidity', 'Foundry', 'Wagmi', 'Vite'],
      category: ['Frontend', 'Backend'],
      year: 'January 2024 - June 2024',
      responsibilities: [
        'Built frontend using React and Tamagui for UI',
        'Fetched data from smart contracts using Solidity, Wagmi, and Viem',
        'Implemented seamless data flow from blockchain to UI',
        'Developed an intuitive user interface for time slot bookings',
        'Ensured secure communication between smart contracts and frontend',
      ],
    },
    {
      name: 'isro-app',
      image: require('app/assets/isro-app.jpg'),
      title: 'NASA Data Visualization Portal',
      description: 'Web app for visualizing NASA space data.',
      goal: 'Provide dynamic visualization of NASA space data in an engaging and interactive manner.',
      achievement:
        'Developed an engaging and interactive data visualization portal that dynamically displays NASA space data.',
      technologyUsed: ['Angular', 'HTML', 'CSS', 'TypeScript', 'NASA APIs'],
      category: ['Frontend', 'Backend'],
      link: 'https://isrospaceinformation.web.app/',
      year: 'June 2022-November 2022',
      live: true,
      responsibilities: [
        'Fetched and processed space data from NASA APIs',
        'Designed and implemented data visualizations using Angular',
        'Enhanced UI for better user engagement and interactivity',
        'Ensured real-time data updates using Angular services',
        'Optimized data rendering for performance on large datasets',
      ],
    },
    {
      name: 'multiplatform.one',
      image: require('app/assets/multiplatform.png'),
      title: 'Multiplatform.One Development Environment',
      description: 'Versatile multi-platform project with modular architecture.',
      goal: 'Create a scalable multi-platform development environment with a versatile and modular architecture.',
      achievement:
        'Developed a comprehensive and modular architecture that supports multiple platforms.',
      technologyUsed: ['Expo', 'Storybook', 'Next.js', 'GraphQL', 'Tamagui'],
      category: ['Frontend', 'Backend'],
      year: '2022-Present',
      responsibilities: [
        'Customized Tamagui components for specific project needs',
        'Published reusable components for use across multiple platforms',
        'Maintained scalable architecture for modularity and versatility',
        'Integrated GraphQL for efficient data fetching',
        'Collaborated with cross-platform teams to ensure compatibility',
      ],
    },
    {
      name: 'selfhosted-cloud',
      image: require('app/assets/selfhosted-cloud.webp'),
      title: 'Self-Hosted Cloud Management System',
      description: 'System for managing Docker environments via GraphQL API.',
      goal: 'Enhance Docker management flexibility and accessibility through a robust system.',
      achievement:
        'Implemented sophisticated API endpoints for executing remote Docker commands, improving management capabilities.',
      technologyUsed: ['GraphQL', 'Docker'],
      category: ['Backend', 'DevOps'],
      year: '2023-Present',
      responsibilities: [
        'Developed API using NestJS to execute Docker commands',
        'Enhanced system flexibility for remote Docker management',
        'Built and tested robust backend API for managing Docker environments',
        'Ensured secure communication between Docker API and management interface',
        'Maintained efficient execution of Docker commands remotely',
      ],
    },
  ],
  services: [
    {
      name: 'Spring Boot Development',
      description:
        'Offering enterprise-grade application development using Spring Boot. Specializing in building scalable back-end systems with Java, Spring, and Hibernate to create efficient, high-performing solutions.',
      logo: require('app/assets/spring-boot-logo.png'),
    },
    {
      name: 'Next.js Development',
      description:
        'Providing dynamic, responsive web application development using Next.js. Leveraging React, TypeScript, and Vercel to deliver high-performance, SEO-friendly, and user-centric web solutions.',
      logo: require('app/assets/nextjs-logo.png'),
    },
    {
      name: 'TypeScript Application Development',
      description:
        'Developing robust and scalable web and server-side applications with TypeScript. Focused on using static typing, interfaces, and decorators for creating maintainable, secure codebases.',
      logo: require('app/assets/typescript-logo.webp'),
    },
    {
      name: 'React Web Development',
      description:
        'Delivering dynamic and interactive web applications using React. Utilizing modern development practices such as hooks, state management, and functional components to create seamless user experiences.',
      logo: require('app/assets/react-logo.png'),
    },
    {
      name: 'JavaScript Development',
      description:
        'Creating interactive and high-performing web applications using JavaScript. Expert in leveraging ES6+, asynchronous programming, and modern JavaScript frameworks for dynamic front-end solutions.',
      logo: require('app/assets/javascript-logo.webp'),
    },
    {
      name: 'Linux System Administration',
      description:
        'Offering Linux system management and command-line interface expertise. Experience includes shell scripting, system monitoring, and basic system administration for efficient server management.',
      logo: require('app/assets/linux-logo.png'),
    },
    {
      name: 'Kubernetes Orchestration',
      description:
        'Providing container orchestration and management services using Kubernetes. Specializing in Helm, Kubectl, and Minikube for scalable, resilient cloud-native application deployments.',
      logo: require('app/assets/kubernate-logo.svg'),
    },
    {
      name: 'Java Enterprise Development',
      description:
        'Offering enterprise-level back-end application development services with Java. Expertise in building secure and efficient systems using Spring, Hibernate, and Maven.',
      logo: require('app/assets/java-logo.jpeg'),
    },
    {
      name: 'Keycloak Integration and IAM',
      description:
        'Specializing in implementing identity and access management solutions using Keycloak. Expertise in SSO, OAuth2, and OpenID Connect to secure applications and manage user access effectively.',
      logo: require('app/assets/keycloak-logo.webp'),
    },
    {
      name: 'NestJS Back-End Development',
      description:
        'Providing scalable back-end application development with NestJS. Leveraging TypeScript, Express, and TypeORM to create secure, high-performance server-side solutions.',
      logo: require('app/assets/nestjs-logo.png'),
    },
    {
      name: 'Angular Web Development',
      description:
        'Building scalable, responsive web applications using Angular. Specializing in TypeScript, RxJS, and Angular Material to create feature-rich, user-friendly web experiences.',
      logo: require('app/assets/angular-logo.png'),
    },
    {
      name: 'Docker Containerization',
      description:
        'Offering application containerization services with Docker. Streamlining development workflows and enhancing application deployments for scalability and efficiency.',
      logo: require('app/assets/docker-logo.webp'),
    },
    {
      name: 'GraphQL API Development',
      description:
        'Building flexible, efficient APIs using GraphQL. Specializing in Apollo Client, TypeGraphQL, and Prisma to create scalable back-end solutions tailored to complex data needs.',
      logo: require('app/assets/graphql-logo.png'),
    },
    {
      name: 'OAuth Security Implementation',
      description:
        'Implementing OAuth authentication and authorization protocols to ensure secure and protected web applications and APIs. Expertise in OAuth2 integration for enhanced user privacy and data security.',
      logo: require('app/assets/oauth-logo.png'),
    },
    {
      name: 'React Native Mobile Development',
      description:
        'Delivering cross-platform mobile applications using React Native. Utilizing Expo, React Navigation, and Redux to create responsive and seamless mobile experiences across iOS and Android.',
      logo: require('app/assets/react-native-logo.svg'),
    },
  ],
}
