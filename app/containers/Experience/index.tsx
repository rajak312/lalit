import { Profile } from 'app/utils'
import { useEffect, useRef } from 'react'
import { SolitoImage } from 'solito/image'
import { Card, H1, H2, Paragraph, Text, YStack, XStack, useMedia, H3, Separator } from 'ui'

export interface ExperienceProps {
  profile: Profile
  onRefChange?: (ref: React.RefObject<HTMLElement>) => void
}

export function Experience({ profile, onRefChange }: ExperienceProps) {
  const ref = useRef(null)
  const { xs } = useMedia()
  const size = xs ? 30 : 48

  useEffect(() => {
    if (!ref.current) return
    onRefChange?.(ref)
  }, [ref.current])

  function calculateExperience(fromDate: string, toDate?: string): string {
    const from = new Date(fromDate)
    const to = toDate ? new Date(toDate) : new Date()

    let years = to.getFullYear() - from.getFullYear()
    let months = to.getMonth() - from.getMonth()

    if (months < 0) {
      years--
      months += 12
    }

    if (years === 0) {
      return `${months} months`
    }
    if (months === 0) {
      return `${years} years`
    }
    return `${years} years, ${months} months`
  }

  return (
    <Card
      alignSelf="center"
      animation="bouncy"
      elevation="$6"
      padding="$4"
      borderRadius="$6"
      marginTop="$8"
      gap="$4"
      $xs={{
        width: '100%',
      }}
      ref={ref}
    >
      {!xs && (
        <Card.Background justifyContent="center">
          <H1 fontSize="$16" rotate="330deg" alignSelf="center" opacity={0.05}>
            Experience
          </H1>
        </Card.Background>
      )}

      <Text fontFamily="$playFlare" fontSize={xs ? '$10' : '$12'} alignSelf="center">
        Experience
      </Text>
      <Separator mb="$2" />

      {profile.experience.map((exp, index) => (
        <XStack key={index} gap="$4" alignItems="flex-start">
          <YStack onPress={() => window.open(exp.url, '_blank')}>
            <SolitoImage
              src={exp.logo}
              width={size}
              height={size}
              alt={`${exp.company} logo`}
              onLayout={() => {}}
              contentFit="cover"
              resizeMode="cover"
              style={{
                borderRadius: 8,
              }}
            />
          </YStack>

          <YStack flex={1} gap="$4">
            <YStack gap="$2">
              <Text fontWeight="bold" fontSize={24} fontFamily="$playFlare">
                {exp.company}
              </Text>
              <Text fontWeight="bold">{exp.location}</Text>
              <Text fontStyle="italic">{calculateExperience(exp.from, exp.to)}</Text>
            </YStack>
            {exp.roles.map((role, roleIndex) => (
              <XStack
                key={roleIndex}
                gap="$4"
                alignItems="flex-start"
                $xs={{
                  gap: '$2',
                }}
              >
                <YStack
                  borderLeftWidth={2}
                  borderColor="$gray9"
                  flex={1}
                  marginRight="$4"
                  paddingVertical="$4"
                  position="relative"
                >
                  <YStack
                    width={12}
                    height={12}
                    borderRadius={6}
                    backgroundColor="$gray9"
                    position="absolute"
                    top={0}
                    left={-6}
                  />
                </YStack>
                <YStack gap="$2" flex={1}>
                  <Text fontWeight="bold" fontSize="$6">
                    {role.role}
                  </Text>
                  <Text fontStyle="italic">
                    {role.from} - {role.to ? role.to : 'Present'}
                  </Text>
                  <Paragraph style={{ flexWrap: 'wrap', wordWrap: 'break-word' }}>
                    {role.description}
                  </Paragraph>

                  {role.skills && (
                    <YStack gap="$1" marginTop="$2">
                      <Text fontWeight="bold">Skills:</Text>
                      <Text>{role.skills.join(', ')}</Text>
                    </YStack>
                  )}
                </YStack>
              </XStack>
            ))}
          </YStack>
        </XStack>
      ))}
    </Card>
  )
}
