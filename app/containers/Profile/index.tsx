import { H1, Paragraph, useMedia, XStack, YStack } from 'ui'
import { AnimatedText } from '../../components/AnimatedText'
import { useEffect, useRef } from 'react'
import { AnimatedStack } from '../../components/AnimatedStack'
import type { Profile } from 'app/utils'
import { SolitoImage } from 'solito/image'

export interface ProfileProps {
  profile: Profile
  onRefChange?: (ref: React.RefObject<HTMLElement>) => void
}

export function Profile({ profile, onRefChange }: ProfileProps) {
  const { xs } = useMedia()
  const profileImage = require('app/assets/profile.png')
  const ref = useRef(null)

  useEffect(() => {
    if (!ref.current) return
    onRefChange?.(ref)
  }, [ref.current])

  return (
    <XStack flexWrap="wrap" justifyContent="space-around" gap="$6" ref={ref}>
      <YStack
        justifyContent="center"
        alignItems="center"
        gap="$4"
        paddingTop="$4"
        $xs={{
          width: '100%',
          gap: '$2',
        }}
      >
        <H1
          fontFamily="$lobster"
          fontSize="$14"
          $xs={{
            fontSize: '$12',
          }}
        >
          I'm
        </H1>
        <H1
          fontFamily="$lobster"
          $xs={{
            fontSize: '$10',
          }}
        >
          {profile.name}
        </H1>

        <Paragraph
          fontFamily="$lobster"
          fontSize="$10"
          marginTop="$4"
          $xs={{
            fontSize: '$9',
            marginTop: '$0',
          }}
        >
          {profile.role}
        </Paragraph>
        <Paragraph
          fontFamily="$lobster"
          fontSize="$7"
          marginHorizontal="$4"
          $xs={{
            fontSize: '$6',
          }}
        >
          {profile.proficiency}
        </Paragraph>
      </YStack>
      <YStack>
        <SolitoImage
          src={profileImage}
          alt=""
          width={xs ? 300 : 400}
          height={xs ? 350 : 500}
          onLayout={() => {}}
          contentFit="fill"
          resizeMode="cover"
          // style={{
          //   borderRadius: 200,
          // }}
        />
        {/* <XStack
            position="absolute"
            gap="$4"
            justifyContent="center"
            alignItems="center"
            borderWidth="$0.25"
            paddingHorizontal="$6"
            borderRadius={9999}
            userSelect="none"
            cursor="pointer"
            left="$-6"
            backgroundColor="$background"
            bottom="$6"
            elevation="$4"
          >
            <XStack alignItems="center">
              <H1 fontFamily="$lobster">{profile.yearOfExperience}</H1>
              <Plus />
            </XStack>
            <YStack>
              <Text>Years of</Text>
              <H4>Experience</H4>
            </YStack>
          </XStack> */}
      </YStack>
    </XStack>
  )
}
