import {
  AlignLeft,
  AlignRight,
  Aperture,
  ChevronRight,
  Clock10,
  Inbox,
  Menu,
  MessageCircleCode,
  Moon,
  Star,
  User2,
} from '@tamagui/lucide-icons'
import { ThemeSwitcher } from 'app/components/ThemeSwitcher'
import { useState } from 'react'
import { Platform } from 'react-native'
import {
  Button,
  H1,
  ListItem,
  Separator,
  SimpleDialog,
  SimplePopover,
  Text,
  useMedia,
  XStack,
  YGroup,
  YStack,
} from 'ui'
export type TabsType = 'about' | 'projects' | 'experience' | 'contact' | 'services'

export interface SideViewProps {
  activeTab?: TabsType
  onTabPress?: (tab: TabsType) => void
}
export function SideView({ onTabPress, activeTab }: SideViewProps) {
  const { xs } = useMedia()
  const [open, setOpen] = useState(false)
  const tabs: TabsType[] = ['about', 'projects', 'services', 'experience', 'contact']
  function handleTabPress(tab: TabsType) {
    if (open) setOpen(false)
    onTabPress?.(tab)
  }

  function renderIcon(tab: TabsType) {
    switch (tab) {
      case 'about':
        return <User2 />
      case 'projects':
        return <Inbox />
      case 'services':
        return <AlignLeft />
      case 'experience':
        return <Aperture />
      case 'contact':
        return <MessageCircleCode />
      default:
        return null
    }
  }

  function renderTabs() {
    return (
      <YGroup bordered size="$5" gap="$2">
        {tabs.map((tab) => (
          <XStack
            padding="$4"
            key={tab}
            alignItems="center"
            gap="$4"
            cursor="pointer"
            userSelect="none"
            elevation="$2"
            hoverStyle={{
              scale: 1.05,
              borderBottomWidth: '$0',
              backgroundColor: '$backgroundHover',
            }}
            onPress={() => handleTabPress(tab)}
            {...(activeTab === tab
              ? {
                  borderBottomWidth: '$0',
                  backgroundColor: '$backgroundFocus',
                }
              : { borderBottomWidth: '$0.25' })}
          >
            {renderIcon(tab)}
            <Text fontSize={xs ? 14 : 28}>{`${tab.charAt(0).toUpperCase()}${tab.slice(1)}`}</Text>
          </XStack>
        ))}
      </YGroup>
    )
  }
  if (xs) {
    return (
      <SimpleDialog
        open={open}
        onOpenChange={setOpen}
        withoutCloseButton
        asRightSideSheet
        trigger={<Button cur="pointer" unstyled icon={<AlignRight size="$3" />} />}
      >
        {renderTabs()}
      </SimpleDialog>
    )
  }
  return (
    <YStack height="100%" justifyContent="center">
      <XStack
        position="absolute"
        top="$8"
        left="$4"
        alignItems="center"
        gap="$4"
        paddingHorizontal="$6"
      >
        <YStack
          position="absolute"
          top={-10}
          left={-20}
          width={50}
          height={50}
          justifyContent="flex-start"
          alignItems="flex-start"
        >
          <YStack borderWidth={4} borderColor="red" width={20} height={0} />
          <YStack borderWidth={4} borderColor="red" height={20} width={0} />
        </YStack>
        <Text fontFamily="$playFlare" fontSize={56}>
          <i>L</i>alit
        </Text>
        <YStack
          position="absolute"
          bottom={-10}
          right={-20}
          width={50}
          height={50}
          justifyContent="flex-end"
          alignItems="flex-end"
        >
          <YStack borderWidth={4} borderColor="red" height={20} width={0} />
          <YStack borderWidth={4} borderColor="red" width={20} height={0} />
        </YStack>
      </XStack>
      {renderTabs()}
    </YStack>
  )
}
