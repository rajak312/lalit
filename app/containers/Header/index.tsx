import { Link } from '@tamagui/lucide-icons'
import { Adapt, Button, Popover, Text, useMedia, XStack, YStack } from 'ui'

export type TabsType = 'about' | 'projects' | 'experience' | 'contact' | 'services'

export interface HeaderProps {
  onTabPres?: (tab: TabsType) => void
}

export function Header({ onTabPres }: HeaderProps) {
  const { xs } = useMedia()
  const tabs: TabsType[] = ['about', 'projects', 'services', 'experience', 'contact']
  function renderTab(tab: TabsType) {
    function handleTab() {
      onTabPres?.(tab)
    }
    return (
      <Text
        hoverStyle={{
          borderBottomWidth: '$1',
          borderColor: '$blue11',
        }}
        cur="pointer"
        paddingBottom="$2"
        onPress={handleTab}
        key={tab}
      >
        {tab.charAt(0).toUpperCase() + tab.slice(1)}
      </Text>
    )
  }

  if (xs) {
    return (
      <Popover size="$5" allowFlip>
        <Popover.Trigger asChild>
          <YStack position="absolute" right="$0" cur="pointer">
            <Link col="blue" zi={999_99} marginRight="$2" marginTop="$2" />
          </YStack>
        </Popover.Trigger>

        <Adapt when="sm" platform="touch">
          <Popover.Sheet modal dismissOnSnapToBottom>
            <Popover.Sheet.Frame padding="$4">
              <Adapt.Contents />
            </Popover.Sheet.Frame>
            <Popover.Sheet.Overlay
              animation="lazy"
              enterStyle={{ opacity: 0 }}
              exitStyle={{ opacity: 0 }}
            />
          </Popover.Sheet>
        </Adapt>

        <Popover.Content
          enterStyle={{ y: -10, opacity: 0 }}
          exitStyle={{ y: -10, opacity: 0 }}
          elevate
          animation={[
            'quick',
            {
              opacity: {
                overshootClamping: true,
              },
            },
          ]}
          gap="$4"
          padding="$4"
          backgroundColor="$background"
        >
          <Popover.Arrow borderWidth={1} borderColor="$borderColor" />
          {tabs.map((tab, key) => (
            <Popover.Close key={key} asChild gap="$2" space="$2">
              <Button>{renderTab(tab)}</Button>
            </Popover.Close>
          ))}
        </Popover.Content>
      </Popover>
    )
  }
  return (
    <XStack justifyContent="flex-end" padding="$2">
      {/* <Text>{profile.name}</Text> */}
      <XStack gap="$4">{tabs.map((tab) => renderTab(tab))}</XStack>
    </XStack>
  )
}
