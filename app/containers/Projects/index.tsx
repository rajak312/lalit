import { Profile } from 'app/utils'
import { useEffect, useRef } from 'react'
import { ScrollView, Separator, Text, useMedia, XStack, YStack } from 'ui'
import { Project } from '../Project'

export interface ProjectsProps {
  profile: Profile
  onRefChange?: (ref: React.RefObject<HTMLElement>) => void
}

export function Projects({ profile, onRefChange }: ProjectsProps) {
  const { xs } = useMedia()
  const ref = useRef(null)

  useEffect(() => {
    if (!ref.current) return
    onRefChange?.(ref)
  }, [ref.current])

  function renderProjects() {
    return profile.projects.map((project) => <Project key={project.title} project={project} />)
  }

  function renderContent() {
    if (xs) {
      return (
        <ScrollView horizontal space="$3" gap="$2" paddingVertical="$4">
          {renderProjects()}
        </ScrollView>
      )
    }
    return (
      <XStack flexWrap="wrap" gap="$4" marginTop="$6" justifyContent="center">
        {renderProjects()}
      </XStack>
    )
  }

  return (
    <YStack marginTop="$8" ref={ref}>
      <Text fontFamily="$playFlare" fontSize={xs ? '$10' : '$11'}>
        Projects
      </Text>
      <Separator marginTop="$6" />
      {renderContent()}
    </YStack>
  )
}
