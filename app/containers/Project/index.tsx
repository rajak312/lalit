import { ArrowUpRight } from '@tamagui/lucide-icons'
import { FlippedCard } from 'app/components/FlippedCard'
import { Profile } from 'app/utils'
import { useEffect, useState } from 'react'
import { Button, ScrollView, SimpleDialog, Text, XStack, YStack } from 'ui'

export function Project({ project }: { project: Profile['projects'][0] }) {
  const [open, setOpen] = useState(false)

  function handleProjectLivePress(url = '') {
    if (!url) return
    return window.open(url, '_blank')
  }

  function handleOpenChange(open: boolean) {
    setOpen(open)
    localStorage.setItem('PROJECT_DIALOG', JSON.stringify(open))
  }

  useEffect(() => {
    if (open) return
    localStorage.removeItem('PROJECT_DIALOG')
  }, [open])

  function renderDialog() {
    return (
      <SimpleDialog
        contentStyle={{
          animation: 'bouncy',
        }}
        open={open}
        onOpenChange={handleOpenChange}
        title={project.title}
        key={project.title}
        description={project.description}
      >
        <ScrollView>
          <Text fontWeight="bold" fontSize="$4">
            Responsibilities:
          </Text>
          <YStack padding="$3" space="$2">
            {project.responsibilities.map((res, index) => (
              <XStack key={index} alignItems="center" space="$2">
                <Text>•</Text>
                <Text>{res}</Text>
              </XStack>
            ))}
          </YStack>
          <Text fontWeight="bold" fontSize="$4">
            Technologies Used:
          </Text>
          <YStack padding="$3" space="$2">
            {project.technologyUsed.map((tech, index) => (
              <XStack key={index} alignItems="center" space="$2">
                <Text>•</Text>
                <Text>{tech}</Text>
              </XStack>
            ))}
          </YStack>
          <XStack justifyContent="flex-end" gap="$4">
            <Button
              bg="$red10Dark"
              hoverStyle={{
                bg: '$red10Dark',
              }}
              borderRadius={999_999}
              gap="$0"
              onPress={() => handleProjectLivePress(project.link)}
            >
              <Text alignSelf="center" color={project.live ? 'red' : undefined}>
                {project.live ? '•' : '🔗'}
              </Text>
              <Text>{project.live ? 'live project' : 'repository'}</Text>
            </Button>
            <Button
              borderRadius={999_999}
              hoverStyle={{
                backgroundColor: '$green10Dark',
              }}
              iconAfter={ArrowUpRight}
              bg="$green10Dark"
            >
              <Text>Project</Text>
            </Button>
          </XStack>
        </ScrollView>
      </SimpleDialog>
    )
  }
  return (
    <YStack>
      <FlippedCard project={project} onDialogOpen={handleOpenChange} />
      {renderDialog()}
    </YStack>
  )
}
