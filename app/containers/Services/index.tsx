import { type Profile } from 'app/utils'
import { useEffect, useRef } from 'react'
import { SolitoImage } from 'solito/image'
import {
  Card,
  H2,
  H3,
  H4,
  H5,
  ScrollView,
  Separator,
  Stack,
  Text,
  useMedia,
  XStack,
  YStack,
} from 'ui'

export interface ServicesProps {
  profile: Profile
  onRefChange?: (ref: React.RefObject<HTMLElement>) => void
}

export function Services({ onRefChange, profile }: ServicesProps) {
  const ref = useRef(null)
  const { xs } = useMedia()

  useEffect(() => {
    if (!ref.current) return
    onRefChange?.(ref)
  }, [ref.current])

  function renderLogo(service: Profile['services'][0]) {
    return (
      <Stack alignItems="center" marginBottom="$4">
        {/* <YStack
          width={80}
          height={0}
          justifyContent="center"
          alignItems="center"
          position="relative"
          borderLeftWidth={40}
          borderLeftColor="transparent"
          borderRightWidth={40}
          borderRightColor="transparent"
          borderBottomWidth={45}
        /> */}
        <Stack width={80} height={45} justifyContent="center" alignItems="center" themeInverse>
          <SolitoImage
            src={service.logo}
            alt={service.name}
            onLayout={() => {}}
            contentFit="cover"
            resizeMode="cover"
            width={40}
            height={40}
            style={{
              borderRadius: 20,
            }}
          />
        </Stack>
        <Stack
          width={80}
          height={0}
          justifyContent="center"
          alignItems="center"
          position="relative"
          borderLeftWidth={40}
          borderLeftColor="transparent"
          borderRightWidth={40}
          borderRightColor="transparent"
          borderTopWidth={45}
          borderTopColor="$backgroundFocus"
        />
      </Stack>
    )
  }

  function renderService() {
    return profile.services.map((service, index) => (
      <Card
        key={index}
        width="30%"
        minWidth="$18"
        padding="$4"
        marginBottom="$6"
        elevation="$4"
        borderRadius="$6"
        $xs={{
          width: '80%',
          padding: '$2',
        }}
      >
        {renderLogo(service)}
        <Stack alignItems="center" marginBottom="$3">
          <H4 textAlign="center" $xs={{ fontSize: 16 }}>
            {service.name}
          </H4>
          <Text textAlign="center" color="gray" $xs={{ fontSize: 12 }} fontSize={16}>
            {service.description}
          </Text>
        </Stack>
      </Card>
    ))
  }

  return (
    <>
      <Text fontFamily="$playFlare" fontSize={xs ? '$10' : '$11'} marginTop="$6">
        Services
      </Text>
      <Separator />
      <ScrollView
        maxHeight={600}
        animation="bouncy"
        padding="$4"
        borderRadius="$6"
        marginTop="$4"
        bg="$backgroundFocus"
        ref={ref}
        $xs={{
          maxHeight: 400,
          borderRadius: '$4',
        }}
        showsHorizontalScrollIndicator={false}
      >
        <XStack flexWrap="wrap" justifyContent="space-around" gap="$4">
          {renderService()}
        </XStack>
      </ScrollView>
    </>
  )
}
