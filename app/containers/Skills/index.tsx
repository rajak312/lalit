import { AnimatedStack } from 'app/components/AnimatedStack'
import { Profile } from 'app/utils'
import { useEffect, useRef } from 'react'
import { SolitoImage } from 'solito/image'
import {
  H1,
  H2,
  H3,
  H4,
  H5,
  Paragraph,
  ScrollView,
  Separator,
  Stack,
  Text,
  useWindowDimensions,
  XStack,
  YStack,
} from 'ui'

export interface SkillsProps {
  profile: Profile
  onRefChange?: (ref: React.RefObject<HTMLElement>) => void
}

export function Skills({ profile, onRefChange }: SkillsProps) {
  const { width } = useWindowDimensions()
  const ref = useRef(null)
  const laptop = require('app/assets/laptop.png')

  useEffect(() => {
    if (!ref.current) return
    onRefChange?.(ref)
  }, [ref.current])

  return (
    <YStack userSelect="none">
      <YStack alignItems="center" justifyContent="center">
        <SolitoImage
          src={laptop}
          alt="Responsive Image"
          onLayout={() => {}}
          width={width / 1.6}
          contentFit="cover"
          resizeMode="cover"
        />
        <XStack position="absolute" top="10%" left="26%" width="48%" height="67%" padding="$4">
          <YStack width="60%" justifyContent="center" alignItems="center" gap="$1">
            <H3>Each skill</H3>
            <H3>reflects my dedication and passion</H3>
            <H3>for building impactful solutions. Let’s create </H3>
            <H3>something great together!</H3>
          </YStack>
          <ScrollView width="40%" showsVerticalScrollIndicator={false}>
            <XStack flexWrap="wrap" gap="$3">
              <AnimatedStack reanimate speed={200}>
                {profile.skills.map((skill, index) => (
                  <XStack
                    justifyContent="center"
                    alignItems="center"
                    key={skill.name}
                    borderRadius="$6"
                    paddingVertical="$4"
                    paddingHorizontal="$3"
                    elevation="$6"
                    backgroundColor="$backgroundStrong"
                    gap="$2"
                    animation="bouncy"
                    hoverStyle={{
                      scale: 1.15,
                    }}
                    pressStyle={{
                      scale: 0.75,
                    }}
                    enterStyle={{
                      x: 100 + index * 100,
                      y: 100 + index * 10,
                    }}
                    cur="pointer"
                    minWidth="$13"
                    $xs={{
                      minWidth: '$12',
                    }}
                  >
                    <Text fontWeight="bold">{skill.name}</Text>
                    <SolitoImage
                      src={skill.logo}
                      width={25}
                      height={25}
                      onLayout={() => {}}
                      contentFit="fill"
                      resizeMode="stretch"
                    />
                  </XStack>
                ))}
              </AnimatedStack>
            </XStack>
          </ScrollView>
        </XStack>
      </YStack>
      <Separator marginVertical="$4" />
    </YStack>
  )
}
