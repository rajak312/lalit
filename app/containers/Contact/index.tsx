import { Mail, MessageSquare, PhoneOutgoing } from '@tamagui/lucide-icons'
import React, { useEffect, useRef, useState } from 'react'
import { Anchor, Button, H1, Input, Text, TextArea, useToastController, XStack, YStack } from 'ui'
import { Github, Gitlab, Linkedin } from '@tamagui/lucide-icons'
import { Profile, Social } from 'app/utils'
import { config } from 'app/config'
import emailjs from 'emailjs-com'
import { DownloadResumeAlert } from 'app/components/DownloadResumeAlert'

export interface ContactProps {
  onRefChange?: (ref: React.RefObject<HTMLElement>) => void
  profile: Profile
}
export interface Error {
  name?: string
  email?: string
  message?: string
}
export function Contact({ onRefChange, profile }: ContactProps) {
  const ref = useRef(null)
  const toastController = useToastController()
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [message, setMessage] = useState('')
  const [disabled, setDisabled] = useState(true)
  const [openAlert, setOpenAlert] = useState(false)
  const [error, setError] = useState<Partial<Error> | undefined>(undefined)
  const github = profile.socials?.find((social) => social.name === Social.Github)
  const gitlab = profile.socials?.find((social) => social.name === Social.Gitlab)
  const linkedin = profile.socials?.find((social) => social.name === Social.LinkedIn)
  const discord = profile.socials?.find((social) => social.name === Social.Discord)
  const skype = profile.socials?.find((social) => social.name === Social.Skype)
  const mail = profile.socials?.find((social) => social.name === Social.Mail)

  useEffect(() => {
    if (
      !name ||
      !email ||
      !message ||
      Object.values(error || {}).filter((val) => val !== '').length
    ) {
      return setDisabled(true)
    }
    return setDisabled(false)
  }, [name, email, message, error])

  useEffect(() => {
    if (!ref.current) return
    onRefChange?.(ref)
  }, [ref.current])

  function handleOpenLink(link: string) {
    window.open(link, '_blank')
  }

  async function handleSendMail() {
    setDisabled(true)
    emailjs
      .send(
        config.EMAILJS_SERVICE_ID,
        config.EMAILJS_TEMPLATE_ID,
        {
          from_name: name,
          from_email: email,
          message: message,
          reply_to: email,
        },
        config.EMAILJS_PUBLIC_KEY
      )
      .then(
        (response) => {
          console.log('SUCCESS!', response.status, response.text)
          toastController.show('email sent')
        },
        (error) => {
          console.log('FAILED...', error)
        }
      )

    setName('')
    setEmail('')
    setMessage('')
    setDisabled(false)
  }

  function handleChangeName(value: string) {
    if (!value || value === '') {
      setError((prev) => ({ ...prev, name: 'name is required' }))
    } else setError((prev) => ({ ...prev, name: '' }))
    setName(value)
  }

  function handleChangeMessage(value: string) {
    if (!value || value === '') {
      setError((prev) => ({ ...prev, message: 'message is required' }))
    } else setError((prev) => ({ ...prev, message: '' }))
    setMessage(value)
  }

  function handleChangeEmail(value: string) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    if (!value || value === '') {
      setError((prev) => ({ ...prev, email: 'email is required' }))
    } else if (!emailRegex.test(value)) {
      setError((prev) => ({ ...prev, email: 'invalid email format' }))
    } else {
      setError((prev) => ({ ...prev, email: '' }))
    }
    setEmail(value)
  }

  return (
    <YStack
      padding="$4"
      width="100%"
      alignSelf="center"
      animation="bouncy"
      marginTop="$8"
      gap="$12"
      justifyContent="space-around"
      $xs={{
        gap: '$4',
      }}
      ref={ref}
    >
      <XStack
        flexWrap="wrap"
        gap="$4"
        padding="$4"
        $xs={{
          padding: '$0',
        }}
      >
        <YStack f={1} gap="$2">
          <H1 marginBottom="$4">Contact Me</H1>
          {mail && (
            <XStack gap="$2">
              <Mail col="$pink8" />
              <Anchor href={mail.link}>{mail.displayName}</Anchor>
            </XStack>
          )}
          {discord && (
            <XStack gap="$2">
              <MessageSquare col="$purple7Light" />
              <Anchor href={discord.link}>{discord.displayName}</Anchor>
            </XStack>
          )}
          {skype && (
            <XStack gap="$2">
              <PhoneOutgoing col="$blue6Dark" />
              <Anchor href={skype.link}>{skype.displayName}</Anchor>
            </XStack>
          )}
          <XStack gap="$3" paddingVertical="$4">
            {linkedin && (
              <YStack cursor="pointer" onPress={() => handleOpenLink(linkedin.link)}>
                <Linkedin size="$2" col="$blue9" />
              </YStack>
            )}
            {gitlab && (
              <YStack cursor="pointer" onPress={() => handleOpenLink(gitlab.link)}>
                <Gitlab size="$2" col="$orange8" />
              </YStack>
            )}
            {github && (
              <YStack cursor="pointer" onPress={() => handleOpenLink(github.link)}>
                <Github size="$2" />
              </YStack>
            )}
          </XStack>
          <DownloadResumeAlert />
        </YStack>
        <YStack gap="$4" f={1}>
          <Input
            borderColor={error?.name ? 'red' : undefined}
            placeholder="Name"
            value={name}
            onChangeText={handleChangeName}
          />
          {error?.name && (
            <Text marginHorizontal="$2" color="red">
              {error.name}
            </Text>
          )}
          <Input
            borderColor={error?.email ? 'red' : undefined}
            placeholder="Email"
            value={email}
            onChangeText={handleChangeEmail}
          />
          {error?.email && (
            <Text marginHorizontal="$2" color="red">
              {error.email}
            </Text>
          )}
          <TextArea
            placeholder="Message"
            borderColor={error?.message ? 'red' : undefined}
            rows={4}
            value={message}
            onChangeText={handleChangeMessage}
          />
          {error?.message && (
            <Text marginHorizontal="$2" color="red">
              {error.message}
            </Text>
          )}
          <Button
            alignSelf="flex-end"
            bg="$green10Dark"
            minWidth="$12"
            $xs={{
              alignSelf: 'auto',
            }}
            hoverStyle={{
              bg: '$green10Dark',
            }}
            onPress={handleSendMail}
            disabled={disabled}
          >
            Submit
          </Button>
        </YStack>
      </XStack>
    </YStack>
  )
}
