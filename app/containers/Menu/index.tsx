import { Github, Gitlab, Link, Linkedin } from '@tamagui/lucide-icons'
import { Profile, Social } from 'app/utils'
import { Adapt, Popover, YStack } from 'ui'

export function Menu({ profile }: { profile: Profile }) {
  const github = profile.socials?.find((social) => social.name === Social.Github)
  const gitlab = profile.socials?.find((social) => social.name === Social.Gitlab)
  const linkedin = profile.socials?.find((social) => social.name === Social.LinkedIn)

  function handleOpenLink(link: string) {
    window.open(link, '_blank')
  }
  return (
    <Popover size="$5" allowFlip>
      <Popover.Trigger asChild>
        <YStack position="absolute" right="$0" cur="pointer">
          <Link col="blue" />
        </YStack>
      </Popover.Trigger>

      <Adapt when="sm" platform="touch">
        <Popover.Sheet modal dismissOnSnapToBottom>
          <Popover.Sheet.Frame padding="$4">
            <Adapt.Contents />
          </Popover.Sheet.Frame>
          <Popover.Sheet.Overlay
            animation="lazy"
            enterStyle={{ opacity: 0 }}
            exitStyle={{ opacity: 0 }}
          />
        </Popover.Sheet>
      </Adapt>

      <Popover.Content
        enterStyle={{ y: -10, opacity: 0 }}
        exitStyle={{ y: -10, opacity: 0 }}
        elevate
        animation={[
          'quick',
          {
            opacity: {
              overshootClamping: true,
            },
          },
        ]}
        gap="$4"
        padding="$4"
        backgroundColor="$background"
      >
        <Popover.Arrow borderWidth={1} borderColor="$borderColor" />
        {linkedin && (
          <YStack cursor="pointer" onPress={() => handleOpenLink(linkedin.link)}>
            <Linkedin size="$2" col="$blue9" />
          </YStack>
        )}
        {gitlab && (
          <YStack cursor="pointer" onPress={() => handleOpenLink(gitlab.link)}>
            <Gitlab size="$2" col="$orange8" />
          </YStack>
        )}
        {github && (
          <YStack cursor="pointer" onPress={() => handleOpenLink(github.link)}>
            <Github size="$2" />
          </YStack>
        )}
      </Popover.Content>
    </Popover>
  )
}
