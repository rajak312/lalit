import { createParam } from 'solito'
import {
  YStack,
  H2,
  Paragraph,
  ScrollView,
  useWindowDimensions,
  XStack,
  Text,
  H4,
  Card,
  H1,
  Button,
} from 'ui'
import { data } from 'app/utils'
import { SolitoImage } from 'solito/image'

const { useParam } = createParam<{ name: string }>()
export function Project() {
  const [name] = useParam('name')
  const { width } = useWindowDimensions()
  const project = data.projects.find((project) => project.name === name)
  if (!name) {
    return <YStack>Project {name} not found</YStack>
  }

  function handelLinkPress() {
    if (project?.link) {
      window.open(project.link, '_blank')
    }
  }

  return (
    <ScrollView paddingVertical="$4" gap="$4">
      <YStack padding="$4">
        <H2 fontFamily="$lobster" fontSize="$10">
          {project?.title}
        </H2>
        <XStack flexWrap="wrap" justifyContent="space-between">
          <Paragraph margin="$2">{project?.description}</Paragraph>
          {project?.link && (
            <YStack paddingHorizontal="$10">
              <Button borderRadius={9999} marginLeft="$4" onPress={handelLinkPress}>
                view project
              </Button>
            </YStack>
          )}
        </XStack>
        <XStack gap="$6" margin="$4" flexWrap="wrap">
          <YStack gap="$2">
            <H4>Year</H4>
            <Text>{project?.year}</Text>
          </YStack>
          <YStack gap="$2">
            <H4>Technology</H4>
            <Text>{project?.technologyUsed.join(', ')}</Text>
          </YStack>
          <YStack gap="$2">
            <H4>category</H4>
            <Text>{project?.category.join(', ')}</Text>
          </YStack>
        </XStack>
      </YStack>
      <YStack $xs={{ paddingLeft: 0 }} gap="$6">
        <Card padding="$4" gap="$4">
          <Card.Background justifyContent="center">
            <H1 fontSize="$12" rotate="330deg" alignSelf="center" opacity={0.05}>
              Goal
            </H1>
          </Card.Background>
          <H1 fontFamily="$lobster">Goal</H1>
          <Paragraph>{project?.goal}</Paragraph>
        </Card>
        <Card padding="$4" gap="$4">
          <Card.Background justifyContent="center">
            <H1 fontSize="$10" rotate="330deg" alignSelf="center" opacity={0.05}>
              Achievement
            </H1>
          </Card.Background>
          <H1 fontFamily="$lobster">Achievement</H1>
          <Paragraph>{project?.achievement}</Paragraph>
        </Card>
      </YStack>
      <SolitoImage
        src={project?.image}
        alt=""
        resizeMode="cover"
        onLayout={() => {}}
        contentFit="fill"
      />
    </ScrollView>
  )
}
