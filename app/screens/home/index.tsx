import { YStack, ScrollView, XStack, Card, SimpleCarousel, useMedia } from 'ui'
import { data as profile } from 'app/utils'
import { useEffect, useState } from 'react'
import { Profile } from 'app/containers/Profile'
import { Skills } from 'app/containers/Skills'
import { Experience } from 'app/containers/Experience'
import { Projects } from 'app/containers/Projects'
import { Header } from 'app/containers/Header'
import { Contact } from 'app/containers/Contact'
import { Services } from 'app/containers/Services'
import { SideView, TabsType } from 'app/containers/SideView'
import { ThemeSwitcher } from 'app/components/ThemeSwitcher'
import { SocialProfile } from 'app/components/SocialProfiles'

export function HomeScreen() {
  const { xs } = useMedia()
  const [aboutRef, setAboutRef] = useState<React.RefObject<HTMLElement> | null>(null)
  const [experienceRef, setExperienceRef] = useState<React.RefObject<HTMLElement> | null>(null)
  const [projectsRef, setProjectsRef] = useState<React.RefObject<HTMLElement> | null>(null)
  const [contactRef, setContactRef] = useState<React.RefObject<HTMLElement> | null>(null)
  const [servicesRef, setServicesRef] = useState<React.RefObject<HTMLElement> | null>(null)
  const [activeTab, setActiveTab] = useState<TabsType | undefined>(undefined)

  useEffect(() => {
    const observerOptions = {
      root: null,
      rootMargin: '0px',
      threshold: 0.5,
    }

    const sections = [
      { ref: aboutRef, tab: 'about' },
      { ref: projectsRef, tab: 'projects' },
      { ref: servicesRef, tab: 'services' },
      { ref: experienceRef, tab: 'experience' },
      { ref: contactRef, tab: 'contact' },
    ]

    const observerCallback = (entries: IntersectionObserverEntry[]) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          const section = sections.find((s) => s.ref?.current === entry.target)
          if (section) {
            setActiveTab(section.tab as TabsType)
          }
        }
      })
    }

    const observer = new IntersectionObserver(observerCallback, observerOptions)

    sections.forEach((section) => {
      if (section.ref?.current) {
        observer.observe(section.ref.current)
      }
    })

    return () => {
      sections.forEach((section) => {
        if (section.ref?.current) {
          observer.unobserve(section.ref.current)
        }
      })
    }
  }, [aboutRef, experienceRef, projectsRef, contactRef, servicesRef])

  function handleTabs(tab: TabsType) {
    switch (tab) {
      case 'about':
        return handleGoToTheTab(aboutRef)
      case 'contact':
        return handleGoToTheTab(contactRef)
      case 'experience':
        return handleGoToTheTab(experienceRef)
      case 'projects':
        return handleGoToTheTab(projectsRef)
      case 'services':
        return handleGoToTheTab(servicesRef)
    }
  }

  function handleGoToTheTab(ref: React.RefObject<HTMLElement> | null) {
    if (!ref) return
    if (!ref.current) return
    ref.current.scrollIntoView({ behavior: 'smooth' })
  }

  return (
    <XStack fullscreen>
      <ThemeSwitcher
        pos="absolute"
        right="$4"
        top="$4"
        $xs={{
          right: undefined,
          left: '$3',
        }}
        zIndex={999_99}
      />
      {!xs && <SocialProfile profile={profile} />}
      <YStack
        padding="$6"
        $xs={{ padding: '$3', position: 'absolute', right: '$0', zIndex: 999_99 }}
      >
        <SideView onTabPress={handleTabs} activeTab={activeTab} />
      </YStack>
      <YStack
        width="80%"
        alignItems="center"
        $xs={{
          width: '100%',
        }}
      >
        <ScrollView
          width="80%"
          als="center"
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          padding="$4"
          $xs={{ width: '100%' }}
        >
          <Profile profile={profile} onRefChange={setAboutRef} />
          <Projects profile={profile} onRefChange={setProjectsRef} />
          <Services profile={profile} onRefChange={setServicesRef} />
          {/* <Skills profile={profile} /> */}
          <Experience profile={profile} onRefChange={setExperienceRef} />
          <Contact profile={profile} onRefChange={setContactRef} />
        </ScrollView>
      </YStack>
    </XStack>
  )
}
