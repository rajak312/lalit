import React, { useState, useEffect, ReactNode } from 'react'

export interface AnimatedStackProps {
  children?: ReactNode[]
  speed?: number
  reanimate?: boolean
}

export const AnimatedStack: React.FC<AnimatedStackProps> = ({
  children = [],
  speed = 100,
  reanimate = false,
}) => {
  const [displayedChildren, setDisplayedChildren] = useState<ReactNode[]>([])
  const [isAnimating, setIsAnimating] = useState<boolean>(false)

  const animateChildren = () => {
    setDisplayedChildren([])
    setIsAnimating(true)
    let currentIndex = 0

    const intervalId = setInterval(() => {
      if (currentIndex < children.length) {
        setDisplayedChildren((prev) => [...prev, children[currentIndex++]])
      } else {
        clearInterval(intervalId)
        setIsAnimating(false)
      }
    }, speed)

    return () => clearInterval(intervalId)
  }

  useEffect(() => {
    if (children.length === 0) return
    animateChildren()
  }, [children, speed])

  const handleMouseEnter = () => {
    if (!isAnimating && reanimate) {
      animateChildren()
    }
  }

  return (
    <>
      {children.map((child, index) => (
        <div key={index} onMouseEnter={handleMouseEnter}>
          {displayedChildren.includes(child) && child}
        </div>
      ))}
    </>
  )
}
