import React, { useState, useEffect } from 'react'

export interface AnimatedTextProps {
  children?: string
  speed?: number
}

export const AnimatedText: React.FC<AnimatedTextProps> = ({ children = '', speed = 100 }) => {
  const [displayedText, setDisplayedText] = useState('')

  useEffect(() => {
    if (children.length === 0) {
      return
    }
    let currentIndex = 0

    const intervalId = setInterval(() => {
      if (currentIndex < children.length) {
        setDisplayedText((prev) => {
          return prev + children[currentIndex++]
        })
      } else {
        clearInterval(intervalId)
      }
    }, speed)

    return () => clearInterval(intervalId)
  }, [children, speed])

  return <>{displayedText}</>
}
