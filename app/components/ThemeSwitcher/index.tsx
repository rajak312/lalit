import { useState } from 'react'
import { Button, useIsomorphicLayoutEffect } from 'tamagui'
import { useThemeSetting, useRootTheme } from '@tamagui/next-theme'
import { Monitor, Moon, Sun } from '@tamagui/lucide-icons'
import type { ButtonProps } from 'ui'

export function ThemeSwitcher({ ...props }: ButtonProps) {
  const themeSetting = useThemeSetting()
  const [theme] = useRootTheme()

  const [clientTheme, setClientTheme] = useState<string | undefined>('dark')

  useIsomorphicLayoutEffect(() => {
    setClientTheme(themeSetting.forcedTheme || themeSetting.current || theme)
  }, [themeSetting.current, themeSetting.resolvedTheme])

  function renderIcon() {
    if (clientTheme === 'dark') return <Moon size="$1.5" />
    if (clientTheme === 'light') return <Sun size="$1.5" />
    return <Monitor size="$1.5" />
  }

  function handlePress(e) {
    e.preventDefault()
    themeSetting.toggle()
    if (props.onPress) props.onPress(e)
  }

  function renderColor() {
    if (clientTheme === 'dark') return '$purple7'
    if (clientTheme === 'light') return '$yellow10'
    return 'darkcyan'
  }

  return (
    <Button
      unstyled
      {...props}
      onPress={handlePress}
      icon={renderIcon()}
      cur="pointer"
      color={renderColor()}
    />
  )
}
