import { X } from '@tamagui/lucide-icons'
import { useState } from 'react'
import { AlertDialog, Button, Unspaced, useMedia, XStack, YStack } from 'ui'

export interface DownloadResumeAlertProps {
  openAlert?: boolean
  setOpenAlert?: (value: boolean) => void
}

export function DownloadResumeAlert({ openAlert, setOpenAlert }: DownloadResumeAlertProps) {
  const [open, setOpen] = useState(!!openAlert)
  function handleDownloadCV() {
    const link = document.createElement('a')
    link.href = '/resume.pdf'
    link.download = 'Lalit_Resume.pdf'
    link.click()
  }

  function handleOpenAlert(value: boolean) {
    setOpen(value)
    if (setOpenAlert) {
      setOpenAlert(value)
    }
  }

  function handleOpenCV() {
    window.open('/resume.pdf', '_blank')
  }

  return (
    <AlertDialog native>
      <AlertDialog.Trigger asChild>
        <Button
          bg="$blue11Dark"
          hoverStyle={{
            bg: '$blue10Dark',
          }}
          als="flex-start"
          $xs={{
            als: 'auto',
          }}
        >
          Resume
        </Button>
      </AlertDialog.Trigger>

      <AlertDialog.Portal>
        <AlertDialog.Overlay
          key="overlay"
          animation="quick"
          opacity={0.5}
          enterStyle={{ opacity: 0 }}
          exitStyle={{ opacity: 0 }}
        />
        <AlertDialog.Content
          bordered
          elevate
          key="content"
          animation={[
            'quick',
            {
              opacity: {
                overshootClamping: true,
              },
            },
          ]}
          enterStyle={{ x: 0, y: -20, opacity: 0, scale: 0.9 }}
          exitStyle={{ x: 0, y: 10, opacity: 0, scale: 0.95 }}
          x={0}
          scale={1}
          opacity={1}
          y={0}
        >
          <YStack space>
            <AlertDialog.Title>Access CV</AlertDialog.Title>
            <AlertDialog.Description>How would you like to view the CV?</AlertDialog.Description>

            <XStack gap="$3" justifyContent="flex-end">
              <AlertDialog.Action onPress={handleOpenCV} asChild>
                <Button>Open Here</Button>
              </AlertDialog.Action>

              <AlertDialog.Action asChild onPress={handleDownloadCV}>
                <Button theme="active">Download CV</Button>
              </AlertDialog.Action>
            </XStack>
          </YStack>
          <Unspaced>
            <AlertDialog.Cancel asChild>
              <Button position="absolute" top="$3" right="$3" size="$2" circular icon={X} />
            </AlertDialog.Cancel>
          </Unspaced>
        </AlertDialog.Content>
      </AlertDialog.Portal>
    </AlertDialog>
  )
}
