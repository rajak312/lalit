import { Profile } from 'app/utils'
import { useEffect, useRef, useState } from 'react'
import { SolitoImage } from 'solito/image'
import { Card, Paragraph, Text, useMedia, XStack, YStack } from 'ui'
import { useThemeSetting } from '@tamagui/next-theme'
import { ArrowUpRight } from '@tamagui/lucide-icons'

export function FlippedCard({
  project,
  onDialogOpen,
}: {
  project: Profile['projects'][0]
  onDialogOpen?: (open: boolean) => void
}) {
  const [flipped, setFlipped] = useState(false)
  const { xs } = useMedia()
  const key = Date.now()
  const cardRef = useRef<HTMLDivElement>(null)
  const themeSetting = useThemeSetting()
  const number = themeSetting.resolvedTheme === 'dark' ? 0 : 252
  const opacity = themeSetting.resolvedTheme === 'dark' ? 0.8 : 0.6
  const width = xs ? 260 : 300
  const height = xs ? 280 : 320
  const darkBg = require('app/assets/dark_background.jpg')
  const lightBg = require('app/assets/light_background.jpg')
  const src = !flipped ? project.image : themeSetting.resolvedTheme === 'dark' ? darkBg : lightBg

  function handleDialogOpen(open: boolean) {
    onDialogOpen?.(open)
    setFlipped(false)
  }

  useEffect(() => {
    function handleMouseMove(event: MouseEvent) {
      if (!cardRef.current) return
      const isDialogOpen = !!JSON.parse(localStorage.getItem('PROJECT_DIALOG') || 'false')
      if (isDialogOpen) return
      const rect = cardRef.current.getBoundingClientRect()
      const mouseX = event.clientX
      const mouseY = event.clientY
      const isOutside =
        mouseX < rect.left || mouseX > rect.right || mouseY < rect.top || mouseY > rect.bottom
      if (isOutside) {
        setFlipped(false)
      } else {
        setFlipped(true)
      }
    }
    document.addEventListener('mousemove', handleMouseMove)
    return () => {
      document.removeEventListener('mousemove', handleMouseMove)
    }
  })

  return (
    <YStack
      key={key}
      width={width}
      minHeight={height}
      animation="lazy"
      elevation="$6"
      borderColor="$background"
      borderRadius="$6"
      ref={cardRef}
      enterStyle={{
        rotateY: `${flipped ? '-' : ''}180deg`,
      }}
      onTouchStart={() => setFlipped(true)}
      style={{
        animationDelay: '5000ms',
      }}
    >
      <Card.Background>
        <YStack>
          <SolitoImage
            src={src}
            alt=""
            width={width}
            height={height}
            onLayout={() => {}}
            contentFit="fill"
            resizeMode="stretch"
            style={{
              borderRadius: 14,
            }}
          />
          <YStack
            pos="absolute"
            bg={`rgba(${number}, ${number}, ${number}, ${flipped ? 0.9 : opacity})`}
            width="100%"
            height="100%"
            alignItems="center"
            justifyContent="center"
          />
        </YStack>
      </Card.Background>
      <YStack
        width={width}
        height={height}
        enterStyle={{
          rotateY: `180deg`,
        }}
        gap="$4"
        padding="$4"
        borderRadius="$4"
        alignItems="center"
        justifyContent="center"
      >
        {!flipped ? (
          <Paragraph
            textAlign="center"
            als="center"
            fontWeight="bold"
            fontFamily="$playFlare"
            fontSize={18}
          >
            {project.title}
          </Paragraph>
        ) : (
          <Text alignSelf="center" textAlign="center" fontSize={24}>
            {project.description}
          </Text>
        )}
      </YStack>
      {flipped && (
        <XStack
          pos="absolute"
          top="$2"
          right="$2"
          padding="$0"
          backgroundColor="$backgroundTransparent"
          gap="$2"
          cur="pointer"
          onPress={handleDialogOpen.bind(null, true)}
        >
          <Text color="blue">more</Text>
          <ArrowUpRight size="$1" color="blue" />
        </XStack>
      )}
    </YStack>
  )
}
