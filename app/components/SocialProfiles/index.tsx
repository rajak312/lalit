import { Github, Gitlab, Linkedin, Mail, MessageSquare, PhoneOutgoing } from '@tamagui/lucide-icons'
import { Profile, Social } from 'app/utils'
import { YStack } from 'ui'

export interface SocialProfileProps {
  profile: Profile
}
export function SocialProfile({ profile }: SocialProfileProps) {
  const github = profile.socials?.find((social) => social.name === Social.Github)
  const gitlab = profile.socials?.find((social) => social.name === Social.Gitlab)
  const linkedin = profile.socials?.find((social) => social.name === Social.LinkedIn)
  const discord = profile.socials?.find((social) => social.name === Social.Discord)
  const skype = profile.socials?.find((social) => social.name === Social.Skype)
  const mail = profile.socials?.find((social) => social.name === Social.Mail)

  function handleOpenLink(link: string) {
    window.open(link, '_blank')
  }

  return (
    <YStack position="absolute" right="$0" top="$0" bottom="$0" justifyContent="center" zi={1_000}>
      <YStack
        gap="$4"
        padding="$4"
        elevation="$4"
        borderTopLeftRadius="$4"
        borderBottomLeftRadius="$4"
      >
        {linkedin && (
          <YStack cursor="pointer" onPress={() => handleOpenLink(linkedin.link)}>
            <Linkedin size="$2" col="$blue9" />
          </YStack>
        )}
        {gitlab && (
          <YStack cursor="pointer" onPress={() => handleOpenLink(gitlab.link)}>
            <Gitlab size="$2" col="$orange8" />
          </YStack>
        )}
        {github && (
          <YStack cursor="pointer" onPress={() => handleOpenLink(github.link)}>
            <Github size="$2" />
          </YStack>
        )}
        {mail && (
          <YStack href={mail.link} cursor="pointer">
            <Mail col="$pink8" />
          </YStack>
        )}
        {discord && (
          <YStack href={discord.link} cursor="pointer">
            <MessageSquare col="$purple7Light" />
          </YStack>
        )}
        {skype && (
          <YStack href={skype.link} cursor="pointer">
            <PhoneOutgoing col="$blue6Dark" />
          </YStack>
        )}
      </YStack>
    </YStack>
  )
}
